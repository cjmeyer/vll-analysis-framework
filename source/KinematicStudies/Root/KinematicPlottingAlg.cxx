#include <AsgTools/MessageCheckAsgTools.h>
#include <KinematicStudies/KinematicPlottingAlg.h>

#include <xAODTruth/TruthParticleContainer.h>
#include <xAODEgamma/PhotonContainer.h>


KinematicPlottingAlg::KinematicPlottingAlg (const std::string& name, ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}



StatusCode KinematicPlottingAlg::initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK( m_systematicsList.initialize() );

  return StatusCode::SUCCESS;
}



StatusCode KinematicPlottingAlg::execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::TruthParticleContainer* photons = nullptr;
  ANA_CHECK (evtStore()->retrieve (photons, "AnalysisTruthPhotons"));

  double lastpt = 1e12;
  for (auto part: *photons) {
    if (part->pt() > lastpt) {
      ANA_MSG_WARNING("Found out-of-order truth photon: " << part->pt() << " > " << lastpt);
    }
  }

  const xAOD::TruthParticleContainer* muons = nullptr;
  ANA_CHECK (evtStore()->retrieve (muons, "AnalysisTruthMuons"));

  lastpt = 1e12;
  for (auto part: *muons) {
    if (part->pt() > lastpt) {
      ANA_MSG_WARNING("Found out-of-order truth muon: " << part->pt() << " > " << lastpt);
    }
    lastpt = part->pt();
  }

//  const xAOD::PhotonContainer* recophotons = nullptr;
//  ANA_CHECK (evtStore()->retrieve (recophotons, "AnalysisPhotons_NOSYS"));
//
//  lastpt = 1e12;
//  for (auto part: *recophotons) {
//    if (part->pt() > lastpt) {
//      ANA_MSG_WARNING("Found out-of-order reco photon: " << part->pt() << " > " << lastpt);
//    }
//    lastpt = part->pt();
//  }

  return StatusCode::SUCCESS;
}



StatusCode KinematicPlottingAlg::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  return StatusCode::SUCCESS;
}
