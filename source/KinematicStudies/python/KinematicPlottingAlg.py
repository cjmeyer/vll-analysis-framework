from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnalysisBaseConfig.AnaConfigFlags import ConfigFlags as flags

import logging
_msg = logging.getLogger('KinematicPlottingAlg')

def makeSequence () :

  algSeq = AlgSequence()

  exampleAlg = createAlgorithm( 'KinematicPlottingAlg', 'KinematicPlottingAlg' )

  algSeq += exampleAlg
  return algSeq
