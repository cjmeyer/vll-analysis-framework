#ifndef KinematicStudies_KinematicPlottingAlg_H
#define KinematicStudies_KinematicPlottingAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>

class KinematicPlottingAlg : public EL::AnaAlgorithm
{
  public:
    // this is a standard algorithm constructor
    KinematicPlottingAlg (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

  private:
    CP::SysListHandle m_systematicsList {this};
};

#endif
