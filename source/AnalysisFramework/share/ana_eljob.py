#!/usr/bin/env python

import logging
_msg = logging.getLogger('vllana_eljob')

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                                      help = 'Submission directory for EventLoop' )
parser.add_option( '-a', '--analysis-sequence', dest = 'analysis_sequence',
                   action = 'store', type = 'string', default = '',
                                      help = 'Sequence of CP and analysis algorithms to schedule' )
parser.add_option( '-i', '--input-dir', dest = 'input_dir',
                   action = 'store', type = 'string', default = '',
                                      help = 'Input directory containing files to run over. If a comma-separated list is passed, each directory is treated as a different sample.' )
parser.add_option( '-I', '--input-file', dest = 'input_file',
                   action = 'store', type = 'string', default = '',
                                      help = 'Input file to run over. If both this and -i are passed, -i will take priority.' )
parser.add_option( '-l', '--input-file-list', dest = 'input_file_list',
                   action = 'store', type = 'string', default = '',
                                      help = 'File containing a list of input files to run over. If both this and -i are passed, -i will take priority.' )
parser.add_option( '-S', '--sample-name', dest = 'sample_name',
                   action = 'store', type = 'string', default = '',
                                      help = 'Name of sample to run over, or comma separated list of samples. If both this and -i/I are passed, -i/I will take priority.' )
parser.add_option( '-n', '--n-events-max', dest = 'n_events_max',
                   action = 'store', type = 'int', default = '-1',
                                      help = 'Maximum number of events to run over.' )
parser.add_option( '-c', '--config-file', dest = 'config_file',
                   action = 'store', type = 'string', default = '',
                                      help = 'Configuration options listed in JSON format which will be added to ConfigFlags object.' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Config flags
from AthenaConfiguration.AllConfigFlags import ConfigFlags

if options.config_file != '':
  from AnalysisFramework.LoadJsonConfigFlags import addJsonFlags
  addJsonFlags(ConfigFlags, options.config_file)

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

inputDirs = []
if options.input_dir != '':
  dirs = options.input_dir.split(',')
  for d in dirs:
    inputDirs.append(d)
elif options.input_file != '':
  sample = ROOT.SH.SampleLocal('file')
  sample.add(options.input_file)
  sh.add(sample)
elif options.input_file_list != '':
  sample = ROOT.SH.SampleLocal('files')
  with open(options.input_file_list) as ifile:
    for line in ifile:
      sample.add(line)
  sh.add(sample)
elif options.sample_name != '':
  from AnalysisFramework.Samples import getSampleDir
  samples = options.sample_name.split(',')  
  for s in samples:
    inputDirs.append(getSampleDir(s))
elif ConfigFlags.hasFlag('Input.Directory'):
  dirs = ConfigFlags.Input.Directory.split(',')
  for d in dirs:
    inputDirs.append(d)
elif ConfigFlags.hasFlag('Input.Directories'):
  inputDirs = ConfigFlags.Input.Directories
elif not '_ATHENA_' in ConfigFlags.Input.Files:
  sample = ROOT.SH.SampleLocal('filelist')
  for f in ConfigFlags.Input.Files:
    sample.add(f)
  sh.add(sample)
else:
  import sys
  _msg.error('No input specified, exiting.')
  sys.exit(1)

if len(inputDirs):
  for i in inputDirs:
    ROOT.SH.ScanDir().scan( sh, i )

print('\n')
sh.printContent()

# Add input files to ConfigFlags so it can look at their meta data
inputfiles = []
for sample in sh:
  for i in range(sample.numFiles()):
    inputfiles.append( sample.fileName(i)[7:] )
ConfigFlags.Input.Files = inputfiles

from AnalysisFramework.AddExtraFlags import addExtraFlags
addExtraFlags(ConfigFlags)

print('\n')
ConfigFlags.dump()
print('\n')

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, options.n_events_max )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')
job.outputAdd( ROOT.EL.OutputStream( 'ANALYSIS' ) )

# Schedule algorithms
if options.analysis_sequence != '':
  algs = options.analysis_sequence.split(',')
elif ConfigFlags.hasFlag('Run.AlgorithmSequence'):
  algs = ConfigFlags.Run.AlgorithmSequence
else:
  import sys
  _msg.error('No algorithms specified, exiting.')
  sys.exit(1)
  
from AnalysisFramework import BaseSequence
algSeq = BaseSequence.makeSequence()

for alg in algs:
  import importlib
  a = importlib.import_module( alg )
  algSeq += a.makeSequence()

print('\n')

# Add scheduled algorithms to job
for alg in algSeq:
  print( alg ) # For debugging
  job.algsAdd( alg )

print('\n')

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
