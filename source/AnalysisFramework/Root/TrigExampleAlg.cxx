#include <AsgTools/MessageCheckAsgTools.h>
#include <AnalysisFramework/TrigExampleAlg.h>

#include <TH1.h>

namespace {
  const static SG::AuxElement::ConstAccessor<char> tight("DFCommonElectronsLHTight");
}

TrigExampleAlg::TrigExampleAlg (const std::string& name, ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
, m_tdt("Trig::TrigDecisionTool/TrigDecisionTool")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("TrigDecisionTool", m_tdt, "The trigger decision tool");
}



StatusCode TrigExampleAlg::initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK( m_systematicsList.addHandle(m_electronHandle) );
  ANA_CHECK( m_systematicsList.initialize() );

  ANA_CHECK( m_tdt.retrieve() );
  ANA_CHECK( m_tmt.retrieve() );

  ANA_CHECK( book( TH1F("et" , "et" , 60, 0, 60) ) ); 
  ANA_CHECK( book( TH1F("et_match" , "et_match" , 60, 0, 60) ) ); 
  ANA_CHECK( book( TH1F("et_match_tool" , "et_match_tool" , 60, 0, 60) ) ); 

  return StatusCode::SUCCESS;
}



StatusCode TrigExampleAlg::execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  std::string tag = "HLT_e26_lhtight_ivarloose_L1EM22VHI";
  std::string probe = "HLT_e26_lhtight_ivarloose_e17_lhvloose_probe_L1EM22VHI";
  std::string onlobjkey = "HLT_egamma_Electrons_GSF";

  // std::string tag = "HLT_e26_lhtight_L1EM22VHI";
  // std::string probe = "HLT_e26_lhtight_e14_etcut_L1EM22VHI";
  // std::string onlobjkey = "HLT_TrigEMClusters_Electrons";

  bool pass_tag_trig = m_tdt->isPassed(tag);
  if (!pass_tag_trig) {
    return StatusCode::SUCCESS;
  }

  const xAOD::ElectronContainer* electrons = nullptr;
  for (const auto& sys : m_systematicsList.systematicsVector()) {
    ANA_CHECK (m_electronHandle.retrieve(electrons, sys));
  }
  // ANA_CHECK (evtStore()->retrieve (electrons, "AnalysisElectrons_NOSYS"));

  if (electrons->size() < 2) {
    return StatusCode::SUCCESS;
  }

  TLorentzVector off_tag = electrons->at(0)->p4();
  TLorentzVector off_probe = electrons->at(1)->p4();

  // Zee mass window
  double mee = (off_tag + off_probe).M();
  if (mee < 81e3 or 101e3 <  mee) {
    return StatusCode::SUCCESS;
  }

  // Opposite sign
  if (electrons->at(0)->charge()*electrons->at(0)->charge() < 0) {
    return StatusCode::SUCCESS;
  }

  // Distinct objects (both online and offline)
  double dR = off_tag.DeltaR(off_probe);
  if (dR < 0.2) {
    return StatusCode::SUCCESS;
  }

  // Tag must be tight
  if (!tight(*electrons->at(0))) {
    return StatusCode::SUCCESS;
  }

  // Tag is matched to e26
  bool pass_tag_match = m_tmt->match(*electrons->at(0), tag);
  if (!pass_tag_match) {
    return StatusCode::SUCCESS;
  }

  hist("et")->Fill(off_probe.Et()/1000.0);

  //-------------- Begin probe analysis
  bool pass_probe_trig = m_tdt->isPassed(probe);
  if (!pass_probe_trig) {
    return StatusCode::SUCCESS;
  }

  // Manual probe trigger match
  auto hltobjs = m_tdt->features<xAOD::ElectronContainer>(probe, TrigDefs::Physics, onlobjkey, TrigDefs::lastFeatureOfType, "feature", 1);

  bool pass_probe_match = false;
  for (auto &onl: hltobjs) {
    if (!onl.isValid()) {
      ANA_MSG_WARNING("in execute, online electron link not valid?");
      continue;
    }

    // Check HLT object
    const auto *elec = *onl.link;
    if (!elec) {
      ANA_MSG_WARNING("in execute, online electron feature not valid?");
      continue;
    }

    TLorentzVector onl_probe_cand = elec->p4();
    if (off_probe.DeltaR(onl_probe_cand) < 0.1) {
      pass_probe_match = true;
      hist("et_match")->Fill(off_probe.Et()/1000.0);
      // ANA_MSG_INFO("Found a match with probe leg!");
      break;
    }
  }

  // Tool probe trigger match (cross-check)
  bool pass_probe_tool_match = m_tmt->match({electrons->at(0), electrons->at(1)}, probe);

  if (pass_probe_tool_match) {
    hist("et_match_tool")->Fill(off_probe.Et()/1000.0);
  }

  if ((pass_probe_match and not pass_probe_tool_match) or (not pass_probe_match and pass_probe_tool_match)) {
    ANA_MSG_INFO("in execute, the manual matching does NOT match the tool matching??");
  }

  return StatusCode::SUCCESS;
}



StatusCode TrigExampleAlg::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  return StatusCode::SUCCESS;
}
