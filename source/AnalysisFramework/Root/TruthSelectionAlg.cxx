#include <AsgTools/MessageCheckAsgTools.h>
#include <AnalysisFramework/TruthSelectionAlg.h>



namespace {
  const static SG::AuxElement::ConstAccessor<unsigned int> origin("classifierParticleOrigin");
}



TruthSelectionAlg::TruthSelectionAlg (const std::string& name, ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty( "PhotonContainerName", m_photonContainerName = "TruthPhotons",
                   "Name of truth photon container" );

  declareProperty( "PhotonPtMin", m_photonPtMin = 10e3,
                   "Minimum photon pT (in MeV)" );
  declareProperty( "PhotonEtaMax", m_photonEtaMax = 2.37,
                   "Maximum photon eta" );

  declareProperty( "PhotonRejectCrack", m_photonRejectCrack = true,
                   "Minimum photon eta" );
  declareProperty( "PhotonCrackEtaMin", m_photonCrackEtaMin = 1.37,
                   "Minimum photon eta" );
  declareProperty( "PhotonCrackEtaMax", m_photonCrackEtaMax = 1.52,
                   "Maximum photon eta" );

  declareProperty( "PhotonOrigin", m_photonOrigin = {},
                   "Truth origin(s) photons must come from" );

  declareProperty( "MuonContainerName", m_muonContainerName = "TruthMuons",
                   "Name of truth photon container" );

  declareProperty( "MuonPtMin", m_muonPtMin = 10e3,
                   "Minimum muon pT (in MeV)" );
  declareProperty( "MuonEtaMax", m_muonEtaMax = 2.7,
                   "Maximum muon eta" );

  declareProperty( "MuonOrigin", m_muonOrigin = {},
                   "Truth origin(s) muons must come from" );

}



StatusCode TruthSelectionAlg::initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK( m_systematicsList.initialize() );

  return StatusCode::SUCCESS;
}



StatusCode TruthSelectionAlg::execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::TruthParticleContainer* truthPhotons = nullptr;
  ANA_CHECK (evtStore()->retrieve (truthPhotons, m_photonContainerName));

  const xAOD::TruthParticleContainer* truthMuons = nullptr;
  ANA_CHECK (evtStore()->retrieve (truthMuons, m_muonContainerName));

  // Sort photons and muons by pT, check that all truth products are available
  auto photons = std::make_unique<ConstDataVector<xAOD::TruthParticleContainer>>(SG::VIEW_ELEMENTS);
  for (auto photon: *truthPhotons) {
    if (photon->pt() < m_photonPtMin) continue;

    double eta = std::abs(photon->eta());
    if (eta > m_photonEtaMax) continue;

    if (m_photonRejectCrack and m_photonCrackEtaMin < eta and eta < m_photonCrackEtaMax) continue;

    if (std::find(m_photonOrigin.begin(), m_photonOrigin.end(), origin(*photon)) == m_photonOrigin.end()) continue;

    photons->push_back(photon);
  }
  photons->sort(comparePt);

  auto muons = std::make_unique<ConstDataVector<xAOD::TruthParticleContainer>>(SG::VIEW_ELEMENTS);
  for (auto muon: *truthMuons) {
    if (muon->pt() < m_muonPtMin) continue;

    if (std::abs(muon->eta()) > m_muonEtaMax) continue;

    if (std::find(m_muonOrigin.begin(), m_muonOrigin.end(), origin(*muon)) == m_muonOrigin.end()) continue;

    muons->push_back(muon);
  }
  muons->sort(comparePt);

  ANA_CHECK (evtStore()->record( photons.release(), "VllTruthPhotons"));
  ANA_CHECK (evtStore()->record( muons.release(), "VllTruthMuons"));

  return StatusCode::SUCCESS;
}



StatusCode TruthSelectionAlg::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  return StatusCode::SUCCESS;
}

bool TruthSelectionAlg::comparePt(const xAOD::TruthParticle *a, const xAOD::TruthParticle *b)
{
  return a->pt() > b->pt();
}

