#ifndef AnalysisFramework_TruthSelectionAlg_H
#define AnalysisFramework_TruthSelectionAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <xAODTruth/TruthParticleContainer.h>

class TruthSelectionAlg : public EL::AnaAlgorithm
{
  public:
    // this is a standard algorithm constructor
    TruthSelectionAlg (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

  private:
    CP::SysListHandle m_systematicsList {this};

    static bool comparePt(const xAOD::TruthParticle*, const xAOD::TruthParticle*);

    std::string m_photonContainerName;
    std::string m_muonContainerName;

    double m_photonPtMin;
    double m_photonEtaMax;

    bool   m_photonRejectCrack;
    double m_photonCrackEtaMin;
    double m_photonCrackEtaMax;

    std::vector<int> m_photonOrigin;

    double m_muonPtMin;
    double m_muonEtaMax;

    std::vector<int> m_muonOrigin;
};

#endif
