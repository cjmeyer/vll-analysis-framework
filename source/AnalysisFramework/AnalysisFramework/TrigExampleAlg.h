#ifndef AnalysisFramework_TrigExampleAlg_H
#define AnalysisFramework_TrigExampleAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>

#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"

#include <xAODEgamma/ElectronContainer.h>

class TrigExampleAlg : public EL::AnaAlgorithm
{
  public:
    // this is a standard algorithm constructor
    TrigExampleAlg (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

  private:
    CP::SysListHandle m_systematicsList {this};

    CP::SysReadHandle<xAOD::ElectronContainer> m_electronHandle
      {this, "ElectronContainer", "AnalysisElectrons_%SYS%", "the electron collection to run on"};

    ToolHandle<Trig::TrigDecisionTool> m_tdt;

    ToolHandle<Trig::IMatchingTool> m_tmt
      {this, "MatchingTool", "Trig::R3MatchingTool","Tool for matching offline objects to trigger objects"};

};

#endif
