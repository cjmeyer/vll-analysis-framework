#ifndef AnalysisFramework_AnalysisFrameworkDict_H
#define AnalysisFramework_AnalysisFrameworkDict_H

// This file includes all the header files that you need to create
// dictionaries for.

#include <AnalysisFramework/ExampleAnalysisAlg.h>
#include <AnalysisFramework/TrigExampleAlg.h>
#include <AnalysisFramework/TruthSelectionAlg.h>
#include <AnalysisFramework/VlxCutBookkeeperAlg.h>

#endif
