from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags

import logging
_msg = logging.getLogger('ExampleAnalysisAlg')

def makeSequence () :

  algSeq = AlgSequence()

  exampleAlg = createAlgorithm( 'ExampleAnalysisAlg', 'ExampleAnalysisAlg' )

  algSeq += exampleAlg
  return algSeq
