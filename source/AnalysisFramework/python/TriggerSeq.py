from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createPublicTool, addPrivateTool
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags

import logging
_msg = logging.getLogger('TriggerSeq')

def makeSequence() :

  algSeq = AnaAlgSequence('TriggerSeq')

  xAODConfTool = createPublicTool( 'TrigConf::xAODConfigTool', 'xAODConfigTool' )
  decisionTool = createPublicTool( 'Trig::TrigDecisionTool', 'TrigDecisionTool')
  decisionTool.ConfigTool = '%s/%s' % ( xAODConfTool.getType(), xAODConfTool.getName() )

  run3_format = flags.Trigger.EDMVersion == 3

  if run3_format:
    decisionTool.NavigationFormat = "TrigComposite"

    from TrigDecisionTool.TrigDecisionToolConfig import getRun3NavigationContainerFromInput
    decisionTool.HLTSummary = getRun3NavigationContainerFromInput(flags)
  else:
    decisionTool.NavigationFormat = "TriggerElement"

  algSeq.addPublicTool( xAODConfTool )
  algSeq.addPublicTool( decisionTool )

  return algSeq
