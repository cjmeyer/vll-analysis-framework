from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createService
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags

import logging
_msg = logging.getLogger('RecoObjectsSequ')

def makeSequence() :

  algSeq = AlgSequence()

  if 'AnalysisMuons' in flags.Input.Collections:
    _msg.info('Analysis objects already available from input file, skipping reco object sequence.')
    return algSeq

  if not 'Muons' in flags.Input.Collections:
    import sys
    _msg.error('Calling reco sequence on file without reco containers, exiting.')
    sys.exit(1)

  dataType = 'data'
  if flags.Input.isMC:
    dataType = 'afii' if flags.Input.isAFII else 'mc'

  # Include, and then set up the electron analysis algorithm sequence:
  doelectrons = True
  if flags.hasFlag('Electrons.MakeContainer'):
    doelectrons = flags.Electrons.MakeContainer

  if doelectrons:
    from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import makeElectronAnalysisSequence
    electronSequence = makeElectronAnalysisSequence( dataType,
                                                     deepCopyOutput = False,
                                                     shallowViewOutput = True,
                                                     crackVeto = True,
                                                     ptSelectionOutput = True,
                                                     recomputeLikelihood = False,
                                                     workingPoint = 'MediumLHElectron.Loose_VarRad' )
    electronSequence.ViewFromSelectionAlg_Electron.sortPt = True
    electronSequence.configure(inputName = 'Electrons', outputName = 'AnalysisElectrons_%SYS%' )
    algSeq += electronSequence

  # Include, and then set up the photon analysis algorithm sequence:
  dophotons = True
  if flags.hasFlag('Photons.MakeContainer'):
    dophotons = flags.Photons.MakeContainer

  if dophotons:
    from EgammaAnalysisAlgorithms.PhotonAnalysisSequence import makePhotonAnalysisSequence
    if '21.2' in flags.Common.Version:
      photonSequence = makePhotonAnalysisSequence( dataType,
                                                   deepCopyOutput = False,
                                                   shallowViewOutput = True,
                                                   recomputeIsEM = False,
                                                   workingPoint = 'Tight.Loose' )

      from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
      alg = createAlgorithm( 'CP::AsgSelectionAlg', 'CutAlg_PhotonPtEta' )
      alg.selectionDecoration = 'selectPtEta,as_bits'
      addPrivateTool( alg, 'selectionTool', 'CP::AsgPtEtaSelectionTool' )
      alg.selectionTool.minPt = 10e3
      alg.selectionTool.maxEta = 2.37
      alg.selectionTool.etaGapLow  = 1.37
      alg.selectionTool.etaGapHigh = 1.52

      photonSequence.append( alg, inputPropName = 'particles',
                             outputPropName = 'particlesOut',
                             stageName = 'selection',
                             metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                                           'selectionDecorNamesOutput' : [alg.selectionDecoration],
                                           'selectionDecorCount' : [1]},
                             dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNames"])} )
    else:
      photonSequence = makePhotonAnalysisSequence( dataType,
                                                   deepCopyOutput = False,
                                                   shallowViewOutput = True,
                                                   crackVeto = True,
                                                   ptSelectionOutput = True,
                                                   recomputeIsEM = False,
                                                   workingPoint = 'Tight.FixedCutLoose' )
    photonSequence.ViewFromSelectionAlg_Photon.sortPt = True
    photonSequence.configure(inputName = 'Photons', outputName = 'AnalysisPhotons_%SYS%' )
    algSeq += photonSequence

  # Include, and then set up the muon analysis algorithm sequence:
  domuons = True
  if flags.hasFlag('Muons.MakeContainer'):
    domuons = flags.Muons.MakeContainer

  if domuons:
    from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
    muonSequence = makeMuonAnalysisSequence( dataType,
                                             deepCopyOutput = False,
                                             shallowViewOutput = True,
                                             workingPoint = 'Medium.Tight_VarRad' )
    muonSequence.ViewFromSelectionAlg_Muon.sortPt = True
    muonSequence.configure( inputName = 'Muons', outputName = 'AnalysisMuons_%SYS%' )
    algSeq += muonSequence

  # Include, and then set up the jet analysis algorithm sequence:
  jetCollection = 'AntiKt4EMPFlowJets'

  dojets = True
  if flags.hasFlag('Jets.MakeContainer'):
    dojets = flags.Jets.MakeContainer

  if dojets:
    from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
    jetSequence = makeJetAnalysisSequence( dataType,
                                           deepCopyOutput = False,
                                           shallowViewOutput = True,
                                           jetCollection = jetCollection )

    # from FTagAnalysisAlgorithms.FTagAnalysisSequence import makeFTagAnalysisSequence
    # makeFTagAnalysisSequence ( jetSequence,
    #                            dataType,
    #                            jetCollection+"_BTagging201903" )

    jetSequence.JetViewFromSelectionAlg.sortPt = True
    jetSequence.configure( inputName = jetCollection, outputName = 'AnalysisJets_%SYS%' )
    algSeq += jetSequence

    # # Include, and then set up the tau analysis algorithm sequence:
    # from TauAnalysisAlgorithms.TauAnalysisSequence import makeTauAnalysisSequence
    # tauSequence = makeTauAnalysisSequence( dataType,
    #                                        deepCopyOutput = True,
    #                                        shallowViewOutput = False,
    #                                        rerunTruthMatching = False,
    #                                        workingPoint = 'Tight' )
    # tauSequence.configure( inputName = 'TauJets', outputName = 'AnalysisTausNoOR_%SYS%' )
    # algSeq += tauSequence

  # Include, and then set up the overlap removal sequence:
  dooverlap = True
  if flags.hasFlag('OverlapRemoval.MakeContainers'):
    dooverlap = flags.OverlapRemoval.MakeContainers

  if dooverlap:
    from AsgAnalysisAlgorithms.OverlapAnalysisSequence import makeOverlapAnalysisSequence
    overlapSequence = makeOverlapAnalysisSequence( dataType,
                                                   doTaus = False,
                                                   enableCutflow = True)
    overlapSequence.configure(
        inputName = {
            'electrons' : 'AnalysisElectrons_%SYS%',
            'photons'   : 'AnalysisPhotons_%SYS%',
            'muons'     : 'AnalysisMuons_%SYS%',
            'jets'      : 'AnalysisJets_%SYS%',
            # 'taus'      : 'AnalysisTausNoOR_%SYS%',
        },
        outputName = {
            'electrons' : 'AnalysisElectronsOR_%SYS%',
            'photons'   : 'AnalysisPhotonsOR_%SYS%',
            'muons'     : 'AnalysisMuonsOR_%SYS%',
            'jets'      : 'AnalysisJetsOR_%SYS%',
            # 'taus'      : 'AnalysisTaus_%SYS%',
        } )
    algSeq += overlapSequence

  return algSeq
