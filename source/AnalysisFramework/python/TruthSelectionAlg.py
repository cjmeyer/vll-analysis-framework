from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags

import logging
_msg = logging.getLogger('TruthSelectionAlg')

def makeSequence () :

  algSeq = AlgSequence()

  if not flags.Input.isMC:
    _msg.warn('Trying to add TruthSelectionAlg to sequence, but not running on MC sample! Skipping.')
    return algSeq

  truthSel = createAlgorithm( 'TruthSelectionAlg', 'TruthSelectionAlg' )

  if flags.hasFlag('Truth.Muons.ContainerName'):
    truthSel.MuonContainerName = flags.Truth.Muons.ContainerName

  if flags.hasFlag('Truth.Muons.PtMin'):
    truthSel.MuonPtMin = flags.Truth.Muons.PtMin

  if flags.hasFlag('Truth.Muons.EtaMax'):
    truthSel.MuonEtaMax = flags.Truth.Muons.EtaMax

  if flags.hasFlag('Truth.Muons.Origin'):
    truthSel.MuonOrigin = flags.Truth.Muons.Origin


  if flags.hasFlag('Truth.Photons.ContainerName'):
    truthSel.PhotonContainerName = flags.Truth.Photons.ContainerName

  if flags.hasFlag('Truth.Photons.PtMin'):
    truthSel.PhotonPtMin = flags.Truth.Photons.PtMin

  if flags.hasFlag('Truth.Photons.EtaMax'):
    truthSel.PhotonEtaMax = flags.Truth.Photons.EtaMax

  if flags.hasFlag('Truth.Photons.RejectCrack'):
    truthSel.PhotonRejectCrack = flags.Truth.Photons.RejectCrack

  if flags.hasFlag('Truth.Photons.CrackEtaMin'):
    truthSel.PhotonCrackEtaMin = flags.Truth.Photons.CrackEtaMin

  if flags.hasFlag('Truth.Photons.CrackEtaMax'):
    truthSel.PhotonCrackEtaMax = flags.Truth.Photons.CrackEtaMax

  if flags.hasFlag('Truth.Photons.Origin'):
    truthSel.PhotonOrigin = flags.Truth.Photons.Origin

  algSeq += truthSel
  return algSeq
