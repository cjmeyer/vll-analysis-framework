import subprocess

_xrootd_server = "eosatlas.cern.ch"

def exist(key, server, type):
   out = subprocess.Popen(["xrd", server, type, key], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
   stdout, stderr = out.communicate()
   if (stdout.find("exists") != -1):
      return True
   return False

def existfile(file, server = _xrootd_server):
   return exist(file, server, "existfile")

def existdir(dir, server = _xrootd_server):
   return exist(dir, server, "existdir")

def mkdir(dir, server = _xrootd_server):
   "Make directory <dir> in xrootd storage at <server>"
   out = subprocess.Popen(["xrd", server, "mkdir", dir], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
   stdout, stderr = out.communicate()

def rm(file, server = _xrootd_server):
   "Remove <file> from xrootd storage at <server>"
   out = subprocess.Popen(["xrd", server, "rm", file], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
   stdout, stderr = out.communicate()

def dirlist(file, server = _xrootd_server):
   "List attributes of <file> from xrootd storage at <server>"
   contents = []
   out = subprocess.Popen(["xrd", server, "ls", file], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
   stdout, stderr = out.communicate()
   for line in stdout.splitlines():
      file = line.split()
      if (len(file) == 5):
         contents.append(file[4])
   return contents

def getFilesInDir(path, server = _xrootd_server):
  if existfile(path, server):
    return [path]
  if existdir(path, server):
    return [f for f in dirlist(path, server) if existfile(f) and f.find("root") != -1 ]

def getFileNameInDir(sample, path, server = _xrootd_server):
  files = [f for f in dirlist(path, server) if f.find("root") != -1 ]
  mysample = "." + sample + "."
  samples = []
  for file in files:
    if mysample in file:
      samples.append(file)
  if len(samples) == 0:
    return ""
  return samples[-1].split("/")[-1]
