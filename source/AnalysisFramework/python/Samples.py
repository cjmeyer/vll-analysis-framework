import logging
_msg = logging.getLogger('Samples')

_sampleDirDict = {
  'mumugamgam': '/P/bolt/data/cjmey/share/data/mc16_13TeV.700249.Sh_2210_mumugammagamma_myy_90_200.deriv.DAOD_HIGG1D1.e8253_s3126_r10201_p4344', 
  'H500e350'  : '/P/bolt/data/cjmey/share/Casey/bigfile',
  'mmgam'     : '/P/bolt/data/cjmey/share/data/mc20_13TeV.700019.Sh_228_mmgamma_pty7_ptV90.deriv.DAOD_PHYS.e7947_s3681_r13145_r13146_p5057',
  'data18'    : '/P/bolt/data/cjmey/share/data/data18_13TeV.00364292.physics_Main.deriv.DAOD_PHYS.r13286_p4910_p5057',
  'H750e500'  : '/P/bolt/data/cjmey/share/Casey/H750e500',
  'H_ZZ': '/P/bolt/data/cjmey/share/VectorLikeLeptonsBkgSamples/H_ZZ',
  'SM_ZZ': '/P/bolt/data/cjmey/share/VectorLikeLeptonsBkgSamples/SM_ZZ',
  'Zmumu': '/P/bolt/data/cjmey/share/VectorLikeLeptonsBkgSamples/Zmumu',
  'H_750_e4_300': '/P/bolt/data/cjmey/share/VectorLikeLeptonsWorkingPoints/H_750_e4_300',
  'H_750_e4_400': '/P/bolt/data/cjmey/share/VectorLikeLeptonsWorkingPoints/H_750_e4_400',
  'H_750_e4_450': '/P/bolt/data/cjmey/share/VectorLikeLeptonsWorkingPoints/H_750_e4_450',
  'H_750_e4_500': '/P/bolt/data/cjmey/share/VectorLikeLeptonsWorkingPoints/H_750_e4_500',
  'H_750_e4_550': '/P/bolt/data/cjmey/share/VectorLikeLeptonsWorkingPoints/H_750_e4_550',
  'H_750_e4_600': '/P/bolt/data/cjmey/share/VectorLikeLeptonsWorkingPoints/H_750_e4_600',

}

def getSampleDir(name):
  if name in _sampleDirDict:
    return _sampleDirDict[name]

  _msg.error('Sample name \'%s\' unknown, expect problems down the road.')
  return ''
