from AthenaConfiguration.AthConfigFlags import AthConfigFlags

import json

def addJsonFlags(flags, configFile):
  with open(configFile, "r") as read_file:
    configs = json.load(read_file)
    for key, value in configs.items():
      flags.addFlag(key, value)
