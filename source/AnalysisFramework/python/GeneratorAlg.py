from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags
from AnalysisFramework.GeneratorAnalysisSequence import makeGeneratorAnalysisSequence

import logging
_msg = logging.getLogger('GeneratorSeq')

def makeSequence () :

  algSeq = AlgSequence()

  if flags.Input.isMC:
    runNumber = 999999 if len(flags.Input.RunNumber) < 1 else flags.Input.RunNumber[0]
    genSeq = makeGeneratorAnalysisSequence( "mc", 
                                            saveCutBookkeepers = True,
                                            runNumber = runNumber ) 
    # genSeq.CutBookkeeperAlg.OutputLevel = 0
    genSeq.CutBookkeeperAlg.stream = flags.Input.Stream

    algSeq += genSeq

  return algSeq
