def addExtraFlags(flags):
  flags.addFlag('Input.isAFII', lambda prevFlags : 'ATL' in str(prevFlags.Sim.ISF.Simulator))

  def _checkVersion():
    import os
    if "AnalysisBase_VERSION" in os.environ:
      return os.environ["AnalysisBase_VERSION"]
    return "0.0.0"
  flags.addFlag('Common.Version', _checkVersion())

  flags.addFlag('Trigger.doOnlineNavigationCompactification', True)
  flags.addFlag('Trigger.doEDMVersionConversion', False)

  def _checkStream():
    from AthenaConfiguration.AutoConfigFlags import GetFileMD
    metadata_items = GetFileMD(flags.Input.Files).get('metadata_items', {})
    for item in metadata_items:
      if 'Stream' in item:
        return item.replace('Stream','')

  flags.addFlag('Input.Stream', _checkStream())

  # temporary fix for TRUTH3 missing FileMetaData
  if flags.Input.Stream == 'DAOD_TRUTH3':
    flags.Input.isMC = True
