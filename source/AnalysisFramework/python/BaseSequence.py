from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createService
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags

def makeSequence () :

  algSeq = AlgSequence()

  # Set up the systematics loader/handler service:
  sysService = createService( 'CP::SystematicsSvc', 'SystematicsSvc', sequence = algSeq )
  if flags.Input.isMC:
      sysService.sigmaRecommended = 1
  
  selService = createService( 'CP::SelectionNameSvc', 'SelectionNameSvc', sequence = algSeq )

  return algSeq
