from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags
from AsgAnalysisAlgorithms.AnalysisObjectSharedSequence import makeSharedObjectSequence

import logging
_msg = logging.getLogger('TruthObjectSeq')

def makeSequence () :

  algSeq = AlgSequence()

  if not flags.Input.isMC:
    _msg.warn('Trying to add TruthObjectSeq to sequence, but not running on MC sample! Skipping.')
    return algSeq

  # Photons
  if flags.hasCategory('Truth.Photons'):
    seq = _makeObjectSequence('TruthPhotons', flags.Truth.Photons)
  else:
    seq = _makeObjectSequence('TruthPhotons')
  algSeq += seq

  # Muons
  if flags.hasCategory('Truth.Muons'):
    seq = _makeObjectSequence('TruthMuons', flags.Truth.Muons)
  else:
    seq = _makeObjectSequence('TruthMuons')
  algSeq += seq

  return algSeq



def _makeObjectSequence (name, flags = None) :
  
  if flags is not None and _hasFlag(flags, 'ContainerName'):
    name = flags.ContainerName

  seq = AnaAlgSequence()

  seq.addMetaConfigDefault ("selectionDecorNames", [])
  seq.addMetaConfigDefault ("selectionDecorNamesOutput", [])
  seq.addMetaConfigDefault ("selectionDecorCount", [])

  # Basic selection
  alg = createAlgorithm( 'CP::AsgSelectionAlg', str('CutAlg' + '_' + name) )
  alg.selectionDecoration = 'selectPtEta,as_bits'
  addPrivateTool( alg, 'selectionTool', 'CP::AsgPtEtaSelectionTool' )

  if flags is not None and _hasFlag(flags, 'PtMin'):
    alg.selectionTool.minPt = flags.PtMin
  if flags is not None and _hasFlag(flags, 'EtaMax'):
    alg.selectionTool.maxEta = flags.EtaMax

  if flags is not None and _hasFlag(flags, 'RejectCrack') and flags.RejectCrack:
    alg.selectionTool.etaGapLow  = flags.CrackEtaMin
    alg.selectionTool.etaGapHigh = flags.CrackEtaMax

  seq.append( alg, inputPropName = 'particles',
              stageName = 'selection',
              metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                            'selectionDecorNamesOutput' : [alg.selectionDecoration],
                            'selectionDecorCount' : [1]},
              dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNames"])} )
 
  # Pt sort
  alg = createAlgorithm( 'CP::AsgViewFromSelectionAlg', str(name + 'ViewFromSelectionAlg') )
  alg.sortPt = True
  seq.append( alg, inputPropName = 'input', outputPropName = 'output',
      stageName = 'selection',
      dynConfig = {'selection' : lambda meta : meta["selectionDecorNamesOutput"][:]} )

  # Name of collections
  inputName = name
  outputName = 'Analysis'+name

  seq.configure(inputName = inputName, outputName = outputName)

  return seq



def _hasFlag (flags, name) :
  
  from AthenaConfiguration.AthConfigFlags import FlagAddress

  base = ''
  cflags = flags
  if isinstance(flags, FlagAddress):
    base = str(getattr(flags, '_name')) + '.'
    cflags = getattr(flags, '_flags')

  if cflags.hasFlag(base+name):
    return True

