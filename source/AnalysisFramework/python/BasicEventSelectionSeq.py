from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createService
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags

import logging
_msg = logging.getLogger('BasicEventSequence')

def makeSequence() :

  algSeq = AlgSequence()

  dataType = 'data'
  if flags.Input.isMC:
    dataType = 'afii' if flags.Input.isAFII else 'mc'

  # Include, and then set up the event selection sequence:
  grlfiles = []
  if not flags.Input.isMC and flags.hasFlag('Event.GRLs'):
    grlfiles = flags.Event.GRLs

  from AsgAnalysisAlgorithms.EventSelectionAnalysisSequence import makeEventSelectionAnalysisSequence
  eventSelectionSequence = makeEventSelectionAnalysisSequence( dataType,
                                                               runPrimaryVertexSelection = True,
                                                               runEventCleaning = False,
                                                               userGRLFiles = grlfiles )

  algSeq += eventSelectionSequence

  # Sequences only needed in MC
  if flags.Input.isMC:

    from AsgAnalysisAlgorithms.GeneratorAnalysisSequence import makeGeneratorAnalysisSequence
    generatorSequence = makeGeneratorAnalysisSequence( dataType,
                                                       saveCutBookkeepers = True,
                                                       runNumber = int(flags.Input.RunNumber[0]) )
    algSeq += generatorSequence

    # Include, and then set up the pileup analysis sequence:
    lumifiles = None
    if flags.hasFlag('Event.LumiCalcFiles'):
      lumifiles = flags.Event.LumiCalcFiles

    pileupconfigs = None
    if flags.hasFlag('Event.PileupConfigs'):
      pileupconfigs = flags.Event.PileupConfigs

    from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
    pileupSequence = makePileupAnalysisSequence( dataType,
                                                 files = flags.Input.Files,
                                                 userLumicalcFiles = lumifiles,
                                                 userPileupConfigs = pileupconfigs )
    pileupSequence.configure( inputName = 'EventInfo', outputName = 'EventInfo_%SYS%' )
    algSeq += pileupSequence

  return algSeq
