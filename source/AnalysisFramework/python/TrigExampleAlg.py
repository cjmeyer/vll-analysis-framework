from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags

import logging
_msg = logging.getLogger('TrigExampleAlg')

def makeSequence () :

  algSeq = AlgSequence()

  alg = createAlgorithm( 'TrigExampleAlg', 'TrigExampleAlg' )

  run3_format = flags.Trigger.EDMVersion == 3

  if run3_format:
    matching_tool = addPrivateTool(alg, "MatchingTool", "Trig::R3MatchingTool")
    addPrivateTool(alg, "MatchingTool.ScoringTool", "Trig::DRScoringTool")
  else:
    matching_tool = addPrivateTool(alg, "MatchingTool", "Trig::MatchingTool")

  algSeq += alg
  return algSeq
