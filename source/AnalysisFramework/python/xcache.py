from rucio.client import Client

class xcache:
  def __init__(self, xcacheServer="bolt.physics.indiana.edu:40735", preferredSite="MWT2_UC_LOCALGROUPDISK"):
    self.xcacheServer = "root://" + xcacheServer + "//"
    self.preferredSite = preferredSite
    self.client = Client()

  def getPFNs(self, sampleName, scope=None):
    pfns = []

    if scope == None:
      scope = sampleName.split(".")[0]

    files = list(self.client.list_files(scope, sampleName))
    xFiles = []
    for f in files:
      xFiles.append({'scope':scope, 'name':f['name']})

    replicas = list(self.client.list_replicas(xFiles, schemes=['root']))

    for rep in replicas:
      if self.preferredSite in rep['rses']:
        pfns.append(rep['rses'][self.preferredSite][0])
      else:
        for site in rep['rses']:
          pfns.append(rep['rses'][site][0])
          break

    return pfns

  def getFiles(self, sampleName, scope=None):
    pfns = self.getPFNs(sampleName, scope)

    xcachePaths = []
    for pfn in pfns:
      xcachePaths.append(self.xcacheServer + pfn)

    return xcachePaths
