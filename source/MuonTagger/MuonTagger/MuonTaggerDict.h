#ifndef MuonTagger_MuonTaggerDict_H
#define MuonTagger_MuonTaggerDict_H

// This file includes all the header files that you need to create
// dictionaries for.

#include <MuonTagger/NTupleMakerAlg.h>
#include <MuonTagger/OnnxTestAlg.h>

#endif
