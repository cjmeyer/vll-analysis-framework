#ifndef MuonTagger_OnnxTestAlg_H
#define MuonTagger_OnnxTestAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <onnxruntime/core/session/experimental_onnxruntime_cxx_api.h>
#include <memory>

#include <memory>

#include "onnxruntime/core/session/experimental_onnxruntime_cxx_api.h"

class OnnxTestAlg : public EL::AnaAlgorithm
{
  public:
    // this is a standard algorithm constructor
    OnnxTestAlg (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

  private:
    StatusCode setDefaults ();
    static bool comparePt (const xAOD::TruthParticle*, const xAOD::TruthParticle*);

    CP::SysListHandle m_systematicsList {this};
    std::unique_ptr<Ort::Experimental::Session> m_session;
    int m_nevents;
    int m_nevents_correct;

};

#endif
