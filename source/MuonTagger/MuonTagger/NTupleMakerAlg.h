#ifndef MuonTagger_NTupleMakerAlg_H
#define MuonTagger_NTupleMakerAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <xAODTruth/TruthParticleContainer.h>

class NTupleMakerAlg : public EL::AnaAlgorithm
{
  public:
    // this is a standard algorithm constructor
    NTupleMakerAlg (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

  private:
    StatusCode setDefaults ();
    static bool comparePt (const xAOD::TruthParticle*, const xAOD::TruthParticle*);

    CP::SysListHandle m_systematicsList {this};
};

#endif
