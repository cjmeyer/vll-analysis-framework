# The name of the package:
atlas_subdir( MuonTagger )

# Add the shared library:
atlas_add_library( MuonTaggerLib
                   MuonTagger/*.h Root/*.cxx
                   PUBLIC_HEADERS MuonTagger
                   LINK_LIBRARIES AnaAlgorithmLib
                                  AsgDataHandlesLib
                                  SystematicsHandlesLib
                                  TrigDecisionToolLib
                                  TriggerMatchingToolLib
                                  xAODEventInfo
                                  xAODMuon
                                  xAODEgamma
                                  onnxruntime )

if (XAOD_STANDALONE)
  # Add the dictionary (for AnalysisBase only):
  atlas_add_dictionary( MuonTaggerDict
                        MuonTagger/MuonTaggerDict.h
                        MuonTagger/selection.xml
                        LINK_LIBRARIES MuonTaggerLib )
else ()
  # Add a component library for Athena based analysis only:
  atlas_add_component( MuonTagger
                       src/components/*.cxx
                       LINK_LIBRARIES MuonTaggerLib )
endif ()

# Install files from the package:
atlas_install_python_modules( python/*.py
                              POST_BUILD_CMD ${ATLAS_FLAKE8} )
