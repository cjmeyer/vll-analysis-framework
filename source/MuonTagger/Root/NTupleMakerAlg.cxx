#include <AsgTools/MessageCheckAsgTools.h>
#include <MuonTagger/NTupleMakerAlg.h>

#include <xAODEventInfo/EventInfo.h>

#include <TH1.h>

namespace {
  const static SG::AuxElement::ConstAccessor<unsigned int> origin("classifierParticleOrigin");
  const static SG::AuxElement::ConstAccessor<int> acc_ise4("mu_e4_index");

  const static SG::AuxElement::Decorator<int>   mu_e4_index("mu_e4_index");

  const static SG::AuxElement::Decorator<float> dphi_mu1_h("dphi_mu1_h");
  const static SG::AuxElement::Decorator<float> dphi_mu2_h("dphi_mu2_h");
  const static SG::AuxElement::Decorator<float> drap_mu1_h("drap_mu1_h");
  const static SG::AuxElement::Decorator<float> drap_mu2_h("drap_mu2_h");

  const static SG::AuxElement::Decorator<float> mu1_pt ("mu1_pt");
  const static SG::AuxElement::Decorator<float> mu1_eta("mu1_eta");
  const static SG::AuxElement::Decorator<float> mu1_phi("mu1_phi");
  const static SG::AuxElement::Decorator<float> mu1_m  ("mu1_m");

  const static SG::AuxElement::Decorator<float> mu2_pt ("mu2_pt");
  const static SG::AuxElement::Decorator<float> mu2_eta("mu2_eta");
  const static SG::AuxElement::Decorator<float> mu2_phi("mu2_phi");
  const static SG::AuxElement::Decorator<float> mu2_m  ("mu2_m");

  const static SG::AuxElement::Decorator<float> ph1_pt ("ph1_pt");
  const static SG::AuxElement::Decorator<float> ph1_eta("ph1_eta");
  const static SG::AuxElement::Decorator<float> ph1_phi("ph1_phi");
  const static SG::AuxElement::Decorator<float> ph1_m  ("ph1_m");

  const static SG::AuxElement::Decorator<float> ph2_pt ("ph2_pt");
  const static SG::AuxElement::Decorator<float> ph2_eta("ph2_eta");
  const static SG::AuxElement::Decorator<float> ph2_phi("ph2_phi");
  const static SG::AuxElement::Decorator<float> ph2_m  ("ph2_m");


  const static SG::AuxElement::Decorator<float> m_H  ("H_m");
  const static SG::AuxElement::Decorator<float> m_e4  ("e4_m");

}


NTupleMakerAlg::NTupleMakerAlg (const std::string& name, ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}



StatusCode NTupleMakerAlg::initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK( m_systematicsList.initialize() );

  ANA_CHECK( book( TH1F("h_mass" , "h_mass" , 100, 100, 150) ) ); 
  ANA_CHECK( book( TH1F("H_mass" , "H_mass" , 100, 400, 600) ) ); 

  return StatusCode::SUCCESS;
}



StatusCode NTupleMakerAlg::execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  const xAOD::TruthParticleContainer* photons = nullptr;
  ANA_CHECK (evtStore()->retrieve (photons, "VllTruthPhotons"));

  const xAOD::TruthParticleContainer* muons = nullptr;
  ANA_CHECK (evtStore()->retrieve (muons, "VllTruthMuons"));

  bool foundH = false, founde4 = false;
  for (auto muon: *muons) {
    if (origin(*muon) == 46)
      founde4 = true;

    if (origin(*muon) == 15)
      foundH = true;
  }

  if (photons->size() != 2 or muons->size() != 2 or not foundH or not founde4) {
    ANA_CHECK( setDefaults() );
    ANA_MSG_DEBUG("Didn't find 2 photons from h or two muons associated with e4 and H");
    return StatusCode::SUCCESS;
  }


  // Basic event objects
  TLorentzVector mu1 = muons->at(0)->p4();
  TLorentzVector mu2 = muons->at(1)->p4();

  TLorentzVector ph1 = photons->at(0)->p4();
  TLorentzVector ph2 = photons->at(1)->p4();

  // Build composite objects
  TLorentzVector h = ph1 + ph2;
  TLorentzVector H = h + mu1 + mu2;

  hist("h_mass")->Fill(h.M()/1000);
  hist("H_mass")->Fill(H.M()/1000);


  // Classifier based on truth info
  TLorentzVector e4; 
  mu_e4_index(*eventInfo) = -1;
  for (size_t i = 0; i < muons->size(); ++i) {
    if ( origin(*muons->at(i)) == 46 ){ 
       mu_e4_index(*eventInfo) = i;
    
       e4 = h+muons->at(i)->p4();
       }
  }

  if (acc_ise4(*eventInfo) < 0) {
    ANA_CHECK( setDefaults() );
    ANA_MSG_DEBUG("Could't find index of muon associated with e4");
    return StatusCode::SUCCESS;
  }


  // Basic angular variables
  dphi_mu1_h(*eventInfo) = mu1.DeltaPhi(h);
  dphi_mu2_h(*eventInfo) = mu2.DeltaPhi(h);

  drap_mu1_h(*eventInfo) = mu1.Rapidity() - h.Rapidity();
  drap_mu2_h(*eventInfo) = mu2.Rapidity() - h.Rapidity();
  
  // Full 4-vector information
  mu1 *= 1/H.M();
  mu1_pt (*eventInfo) = mu1.Pt();
  mu1_eta(*eventInfo) = mu1.Eta();
  mu1_phi(*eventInfo) = mu1.Phi();
  mu1_m  (*eventInfo) = mu1.M();

  mu2 *= 1/H.M();
  mu2_pt (*eventInfo) = mu2.Pt();
  mu2_eta(*eventInfo) = mu2.Eta();
  mu2_phi(*eventInfo) = mu2.Phi();
  mu2_m  (*eventInfo) = mu2.M();

  ph1 *= 1/H.M();
  ph1_pt (*eventInfo) = ph1.Pt();
  ph1_eta(*eventInfo) = ph1.Eta();
  ph1_phi(*eventInfo) = ph1.Phi();
  ph1_m  (*eventInfo) = ph1.M();

  ph2 *= 1/H.M();
  ph2_pt (*eventInfo) = ph2.Pt();
  ph2_eta(*eventInfo) = ph2.Eta();
  ph2_phi(*eventInfo) = ph2.Phi();
  ph2_m  (*eventInfo) = ph2.M();

  
 // m_H (*eventInfo) = H.M()/1000;
  m_H (*eventInfo) = 1.0;
  //m_e4 (*eventInfo) = e4.M()/1000;
  e4 *= 1/H.M(); 
  m_e4 (*eventInfo) = e4.M();


  return StatusCode::SUCCESS;
}



StatusCode NTupleMakerAlg::setDefaults ()
{
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // Classifier
  mu_e4_index(*eventInfo) = -1;

  // Basic angular variables
  dphi_mu1_h(*eventInfo) = 0;
  dphi_mu2_h(*eventInfo) = 0;

  drap_mu1_h(*eventInfo) = 0;
  drap_mu2_h(*eventInfo) = 0;
  
  // Full 4-vector information
  mu1_pt (*eventInfo) = 0;
  mu1_eta(*eventInfo) = 0;
  mu1_phi(*eventInfo) = 0;
  mu1_m  (*eventInfo) = 0;

  mu2_pt (*eventInfo) = 0;
  mu2_eta(*eventInfo) = 0;
  mu2_phi(*eventInfo) = 0;
  mu2_m  (*eventInfo) = 0;

  ph1_pt (*eventInfo) = 0;
  ph1_eta(*eventInfo) = 0;
  ph1_phi(*eventInfo) = 0;
  ph1_m  (*eventInfo) = 0;

  ph2_pt (*eventInfo) = 0;
  ph2_eta(*eventInfo) = 0;
  ph2_phi(*eventInfo) = 0;
  ph2_m  (*eventInfo) = 0;

  m_H (*eventInfo) = 0;
  m_e4 (*eventInfo) = 0;

  return StatusCode::SUCCESS;
}



StatusCode NTupleMakerAlg::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  return StatusCode::SUCCESS;
}
