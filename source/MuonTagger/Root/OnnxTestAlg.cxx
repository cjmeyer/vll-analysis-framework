#include <AsgTools/MessageCheckAsgTools.h>
#include <MuonTagger/OnnxTestAlg.h>
#include <xAODEventInfo/EventInfo.h>
#include <vector>
#include <typeinfo>
#include <TH1.h>

namespace {
  const static SG::AuxElement::ConstAccessor<unsigned int> origin("classifierParticleOrigin");
  constexpr double pi = 3.14159265358979323846;
}

// pretty prints a shape dimension vector
std::string print_shape(const std::vector<int64_t>& v) {
  std::stringstream ss("");
  for (size_t i = 0; i < v.size() - 1; i++)
    ss << v[i] << "x";
  ss << v[v.size() - 1];
  return ss.str();
}


OnnxTestAlg::OnnxTestAlg (const std::string& name, ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}



StatusCode OnnxTestAlg::initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK( m_systematicsList.initialize() );

  ANA_CHECK( book( TH1F("h_mass" , "h_mass" , 100, 100, 150) ) ); 
  ANA_CHECK( book( TH1F("H_mass" , "H_mass" , 100, 400, 600) ) ); 

  ANA_CHECK( book( TH1F("mu1_pt"  , "mu1_pt"  , 100, 0   , 1e3) ) ); 
  ANA_CHECK( book( TH1F("mu1_eta" , "mu1_eta" , 100, -3.0, 3.0) ) ); 
  ANA_CHECK( book( TH1F("mu1_phi" , "mu1_phi" , 100, 400 , 600) ) ); 
  ANA_CHECK( book( TH1F("mu1_m"   , "mu1_m"   ,  20, -pi, pi) ) ); 

  ANA_CHECK( book( TH1F("mu2_pt"  , "mu2_pt"  , 100, 0   , 1e3) ) ); 
  ANA_CHECK( book( TH1F("mu2_eta" , "mu2_eta" , 100, -3.0, 3.0) ) ); 
  ANA_CHECK( book( TH1F("mu2_phi" , "mu2_phi" , 100, 400 , 600) ) ); 
  ANA_CHECK( book( TH1F("mu2_m"   , "mu2_m"   ,  20, -pi, pi) ) ); 

  ANA_CHECK( book( TH1F("ph1_pt"  , "ph1_pt"  , 100, 0   , 1e3) ) ); 
  ANA_CHECK( book( TH1F("ph1_eta" , "ph1_eta" , 100, -3.0, 3.0) ) ); 
  ANA_CHECK( book( TH1F("ph1_phi" , "ph1_phi" , 100, 400 , 600) ) ); 
  ANA_CHECK( book( TH1F("ph1_m"   , "ph1_m"   ,  20, -pi, pi) ) ); 

  ANA_CHECK( book( TH1F("ph2_pt"  , "ph2_pt"  , 100, 0   , 1e3) ) ); 
  ANA_CHECK( book( TH1F("ph2_eta" , "ph2_eta" , 100, -3.0, 3.0) ) ); 
  ANA_CHECK( book( TH1F("ph2_phi" , "ph2_phi" , 100, 400 , 600) ) ); 
  ANA_CHECK( book( TH1F("ph2_m"   , "ph2_m"   ,  20, -pi, pi) ) ); 

  // onnxruntime setup
  std::string model_file = "model.onnx";
  Ort::Env env(ORT_LOGGING_LEVEL_WARNING, "example-model-explorer");
  Ort::SessionOptions session_options;

  m_session = std::make_unique<Ort::Experimental::Session>(env, model_file, session_options);

  // prints out shape of input layer, simple check to see if model is loaded correctly
  std::vector<std::string> input_names = m_session->GetInputNames();
  std::vector<std::vector<int64_t> > input_shapes = m_session->GetInputShapes();
  std::cout << "Input Node Name/Shape (" << input_names.size() << "):" << std::endl;
  for (size_t i = 0; i < input_names.size(); i++) {
    std::cout << "\t" << input_names[i] << " : " << print_shape(input_shapes[i]) << std::endl;
  }

  m_nevents=0.0;
  m_nevents_correct=0.0;

  return StatusCode::SUCCESS;
}



StatusCode OnnxTestAlg::execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::TruthParticleContainer* photons = nullptr;
  ANA_CHECK (evtStore()->retrieve (photons, "VllTruthPhotons"));

  const xAOD::TruthParticleContainer* muons = nullptr;
  ANA_CHECK (evtStore()->retrieve (muons, "VllTruthMuons"));

  bool foundH = false, founde4 = false;
  for (auto muon: *muons) {
    if (origin(*muon) == 46)
      founde4 = true;

    if (origin(*muon) == 15)
      foundH = true;
  }

  if (photons->size() != 2 or muons->size() != 2 or not foundH or not founde4) {
    ANA_MSG_DEBUG("Didn't find 2 photons from h or two muons associated with e4 and H");
    return StatusCode::SUCCESS;
  }


  // Basic event objects
  TLorentzVector mu1 = muons->at(0)->p4();
  TLorentzVector mu2 = muons->at(1)->p4();

  TLorentzVector ph1 = photons->at(0)->p4();
  TLorentzVector ph2 = photons->at(1)->p4();

  // Build composite objects
  TLorentzVector h = ph1 + ph2;
  TLorentzVector H = h + mu1 + mu2;

  hist("h_mass")->Fill(h.M()/1000);
  hist("H_mass")->Fill(H.M()/1000);

  // Classifier based on truth info
  TLorentzVector e4; 
  int mu_e4_index = -1;
  for (size_t i = 0; i < muons->size(); ++i) {
    if ( origin(*muons->at(i)) == 46 ){ 
       mu_e4_index = i;
    
       e4 = h+muons->at(i)->p4();
       }
  }

  if (mu_e4_index < 0) {
    ANA_MSG_DEBUG("Could't find index of muon associated with e4");
    return StatusCode::SUCCESS;
  }

  // 4-vector inputs
  double mu1_pt = 0.0, mu1_eta = 0.0, mu1_phi = 0.0, mu1_m = 0.0;
  mu1 *= 1/H.M();
  mu1_pt  = mu1.Pt();
  mu1_eta = mu1.Eta();
  mu1_phi = mu1.Phi();
  mu1_m   = mu1.M();

  hist("mu1_pt")->Fill(mu1_pt/1000);
  hist("mu1_eta")->Fill(mu1_eta/1000);
  hist("mu1_phi")->Fill(mu1_phi/1000);
  hist("mu1_m")->Fill(mu1_m/1000);

  double mu2_pt = 0.0, mu2_eta = 0.0, mu2_phi = 0.0, mu2_m = 0.0;
  mu2 *= 1/H.M();
  mu2_pt  = mu2.Pt();
  mu2_eta = mu2.Eta();
  mu2_phi = mu2.Phi();
  mu2_m   = mu2.M();

  hist("mu2_pt")->Fill(mu2_pt/1000);
  hist("mu2_eta")->Fill(mu2_eta/1000);
  hist("mu2_phi")->Fill(mu2_phi/1000);
  hist("mu2_m")->Fill(mu2_m/1000);

  double ph1_pt = 0.0, ph1_eta = 0.0, ph1_phi = 0.0, ph1_m = 0.0;
  ph1 *= 1/H.M();
  ph1_pt  = ph1.Pt();
  ph1_eta = ph1.Eta();
  ph1_phi = ph1.Phi();
  ph1_m   = ph1.M();

  hist("ph1_pt")->Fill(ph1_pt/1000);
  hist("ph1_eta")->Fill(ph1_eta/1000);
  hist("ph1_phi")->Fill(ph1_phi/1000);
  hist("ph1_m")->Fill(ph1_m/1000);

  double ph2_pt = 0.0, ph2_eta = 0.0, ph2_phi = 0.0, ph2_m = 0.0;
  ph2 *= 1/H.M();
  ph2_pt  = ph2.Pt();
  ph2_eta = ph2.Eta();
  ph2_phi = ph2.Phi();
  ph2_m   = ph2.M();

  hist("ph2_pt")->Fill(ph2_pt/1000);
  hist("ph2_eta")->Fill(ph2_eta/1000);
  hist("ph2_phi")->Fill(ph2_phi/1000);
  hist("ph2_m")->Fill(ph2_m/1000);

  //e4 *= 600/750; 
  e4 *= 1/H.M(); 
  double m_e4 = e4.M();
  // Here you will pass the above variables to the ONNX model for evaluation
  // and compare how often mu_e4_index (true matched muon) agrees with result

  // puts data in proper format for reading into onnx network
  std::vector<std::vector<int64_t> > input_shapes = m_session->GetInputShapes();
  auto input_shape = input_shapes[0];
  input_shape[0] = input_shape[0]*-1;
  std::vector<float> input_tensor_values(0);
  input_tensor_values.push_back((float) mu1_pt);
  input_tensor_values.push_back((float) mu1_eta);
  input_tensor_values.push_back((float) mu1_phi);
  input_tensor_values.push_back((float) mu1_m);
  input_tensor_values.push_back((float) mu2_pt);
  input_tensor_values.push_back((float) mu2_eta);
  input_tensor_values.push_back((float) mu2_phi);
  input_tensor_values.push_back((float) mu2_m);
  input_tensor_values.push_back((float) ph1_pt);
  input_tensor_values.push_back((float) ph1_eta);
  input_tensor_values.push_back((float) ph1_phi);
  input_tensor_values.push_back((float) ph1_m);
  input_tensor_values.push_back((float) ph2_pt);
  input_tensor_values.push_back((float) ph2_eta);
  input_tensor_values.push_back((float) ph2_phi);
  input_tensor_values.push_back((float) ph2_m);
  input_tensor_values.push_back((float) m_e4);
  std::vector<Ort::Value> input_tensors;
  input_tensors.push_back(Ort::Experimental::Value::CreateTensor<float>(input_tensor_values.data(), input_tensor_values.size(), input_shape));

  //reads input data into network
  //if node2_out is greater, mu1 is from the e4
  //if node1_out is greater, mu2 is from the e4
  try {
    auto output_tensors = m_session->Run(m_session->GetInputNames(), input_tensors, m_session->GetOutputNames());
    Ort::AllocatorWithDefaultOptions allocator;
    float node1_out = output_tensors[0].GetTensorMutableData<float>()[0];
    float node2_out = output_tensors[0].GetTensorMutableData<float>()[1];
    if(mu_e4_index != -1){
      m_nevents++;
      if(((node1_out<node2_out) && mu_e4_index == 0) || ((node1_out>node2_out) && mu_e4_index == 1)){
        m_nevents_correct++;
      }
    }
  } catch (const Ort::Exception& exception) {
    std::cout << "ERROR running model inference: " << exception.what() << std::endl;
    exit(-1);
  }

  return StatusCode::SUCCESS;
}



StatusCode OnnxTestAlg::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  std::cout << "Correct Events: "<< m_nevents_correct << std::endl;
  std::cout << "Total Events: "<< m_nevents << std::endl;

  return StatusCode::SUCCESS;
}
