from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm

def makeSequence () :

  algSeq = AlgSequence()

  # Algorithm for calculating variables to be dumped
  ntupAlg = createAlgorithm( 'NTupleMakerAlg', 'NTupleMakerAlg' )
  algSeq += ntupAlg

  treename = 'muontag'

  # Add an ntuple dumper algorithm:
  treeMaker = createAlgorithm( 'CP::TreeMakerAlg', 'TreeMaker' )
  treeMaker.TreeName = treename
  algSeq += treeMaker

  ntupleMaker = createAlgorithm( 'CP::AsgxAODNTupleMakerAlg', 'NTupleMakerEventInfo' )
  ntupleMaker.TreeName = treename
  ntupleMaker.Branches = [ 
                           'EventInfo.mu_e4_index    -> mu_e4_index',

                           'EventInfo.dphi_mu1_h     -> dphi_mu1_h',
                           'EventInfo.dphi_mu2_h     -> dphi_mu2_h',
                           'EventInfo.drap_mu1_h     -> drap_mu1_h',
                           'EventInfo.drap_mu2_h     -> drap_mu2_h',

                           'EventInfo.mu1_pt         -> mu1_pt'    ,
                           'EventInfo.mu1_eta        -> mu1_eta'   ,
                           'EventInfo.mu1_phi        -> mu1_phi'   ,
                           'EventInfo.mu1_m          -> mu1_m'     ,

                           'EventInfo.mu2_pt         -> mu2_pt'    ,
                           'EventInfo.mu2_eta        -> mu2_eta'   ,
                           'EventInfo.mu2_phi        -> mu2_phi'   ,
                           'EventInfo.mu2_m          -> mu2_m'     ,

                           'EventInfo.ph1_pt         -> ph1_pt'    ,
                           'EventInfo.ph1_eta        -> ph1_eta'   ,
                           'EventInfo.ph1_phi        -> ph1_phi'   ,
                           'EventInfo.ph1_m          -> ph1_m'     ,

                           'EventInfo.ph2_pt         -> ph2_pt'    ,
                           'EventInfo.ph2_eta        -> ph2_eta'   ,
                           'EventInfo.ph2_phi        -> ph2_phi'   ,
                           'EventInfo.ph2_m          -> ph2_m'     ,


                           'EventInfo.H_m          -> H_m'     ,
                           'EventInfo.e4_m          -> e4_m'     ,
                         ]
  algSeq += ntupleMaker

  treeFiller = createAlgorithm( 'CP::TreeFillerAlg', 'TreeFiller' )
  treeFiller.TreeName = treename
  algSeq += treeFiller

  return algSeq
