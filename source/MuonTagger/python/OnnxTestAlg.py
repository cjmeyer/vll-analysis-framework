from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm

def makeSequence () :

  algSeq = AlgSequence()

  # Algorithm for calculating variables to be dumped
  onnxAlg = createAlgorithm( 'OnnxTestAlg', 'OnnxTestAlg' )
  algSeq += onnxAlg

  return algSeq
