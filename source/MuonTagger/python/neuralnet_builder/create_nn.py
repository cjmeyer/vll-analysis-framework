import ROOT
from root_numpy import testdata, root2array, tree2array
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np

simple_input = ["dphi_mu1_h", "dphi_mu2_h", "drap_mu1_h", "drap_mu2_h"]
complex_input1 = ["mu1_pt", "mu1_eta", "mu1_phi", "mu1_m", "mu2_pt", "mu2_eta", "mu2_phi", "mu2_m", "ph1_pt", "ph1_eta", "ph1_phi", "ph1_m", "ph2_pt", "ph2_eta", "ph2_phi", "ph2_m"]
complex_input2 = ["mu1_pt", "mu1_eta", "mu1_phi", "mu1_m", "mu2_pt", "mu2_eta", "mu2_phi", "mu2_m", "ph1_pt", "ph1_eta", "ph1_phi", "ph1_m", "ph2_pt", "ph2_eta", "ph2_phi", "ph2_m", "H_m", "e4_m"]
complex_input3 = ["mu1_pt", "mu1_eta", "mu1_phi", "mu1_m", "mu2_pt", "mu2_eta", "mu2_phi", "mu2_m", "ph1_pt", "ph1_eta", "ph1_phi", "ph1_m", "ph2_pt", "ph2_eta", "ph2_phi", "ph2_m", "e4_m"]

complex_input = complex_input3

#Open Root file
rfile = ROOT.TFile.Open("H_750_e4_300_400_450_550_600.root")

#Get Muontag Ttree from Root file
intree = rfile.Get("muontag")

print("reading")

#
input_starter_array = tree2array(intree, branches = complex_input, selection = "mu_e4_index != -1")
input_array=[]
for i in range(input_starter_array.size):
  input_array.append(list(input_starter_array[i]))
input_array=np.array(input_array)
output_starter_array = tree2array(intree, branches = ["mu_e4_index"], selection = "mu_e4_index != -1")
output_array = np.eye(input_array.shape[0], 2)
for i in range(input_array.shape[0]):
  output_array[i,0] = output_starter_array[i][0]
  output_array[i,1] = 1-output_starter_array[i][0]
input_layer = keras.Input(shape=(len(complex_input),))
x=layers.Dense(30, activation="relu")(input_layer)
x=layers.Dense(20, activation="relu")(x)
x=layers.Dense(15, activation="relu")(x)
x=layers.Dense(5, activation="sigmoid")(x)
output_layer = layers.Dense(2)(x)
model = keras.Model(input_layer, output_layer, name = "simple_muon_tagging")

model.compile(
  loss="binary_crossentropy",
  optimizer=tf.keras.optimizers.Adam(lr=0.01),
  metrics=[keras.metrics.BinaryAccuracy(name="binary_accuracy", threshold=0.5)]
  )

history = model.fit(input_array, output_array, batch_size=64, epochs=50, validation_split=0.2)
model.save("model_file")
