# Neuralnet-builder

Contains all the code necessary to create a neural network for muon tagging

## Creating the network

The following code will create the network and save it in tensorflow format
```
source setup_nn_environment.sh
python create_nn.py
```
This will save the model to the saved-model directory in tensorflow format. However, in order to load into the C++ code, the model must be in ONNX format

```
python -m venv ~/onnx-venv
source ~/onnx-venv/bin/activate
pip install --upgrade pip
export PYTHONPATH=$VIRTUAL_ENV/lib/python3.7/site-packages:$PYTHONPATH
```
Every time you log in after that, you’ll need to do commands 2 and 4 again. You might be able to add them to `setup_nn_environment.sh`.

Now you can you use the python script `convertTensorFlow_2_Onnx.py` to convert the tensor file to an onnx file

This will save the model to model.onnx, which can then be included in C++ code
