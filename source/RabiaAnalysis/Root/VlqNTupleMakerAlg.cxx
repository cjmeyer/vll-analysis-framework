#include <AsgTools/MessageCheckAsgTools.h>
#include <RabiaAnalysis/VlqNTupleMakerAlg.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthMetaData.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODCutFlow/CutBookkeeperContainer.h>
#include <xAODCutFlow/CutBookkeeper.h>
#include <TH1.h>
#include <TH2.h>
#include <xAODJet/JetContainer.h>
#include <xAODBTagging/BTagging.h>
#include "TGraph.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TGaxis.h"

namespace {
  const static SG::AuxElement::ConstAccessor<unsigned int> origin("classifierParticleOrigin");
  const static SG::AuxElement::Decorator<float> Pt_j1("Pt_j1");
  const static SG::AuxElement::Decorator<float> Eta_j1("Eta_j1");
  const static SG::AuxElement::Decorator<int> num_b("num_b");
  const static SG::AuxElement::Decorator<float> Met("Met");
  const static SG::AuxElement::Decorator<float> HTb("HTb");
  const static SG::AuxElement::Decorator<float> Et("Et");
}


VlqNTupleMakerAlg::VlqNTupleMakerAlg(const std::string& name, ISvcLocator* pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  //   // e.g. initialize all pointers to 0.  This is also where you
  //     // declare all properties for your algorithm.  Note that things like
  //       // resetting statistics variables or booking histograms should
  //         // rather go into the initialize() function.
 }



//----------------  (INITIALISE) -----------------------------------------------------------------

StatusCode VlqNTupleMakerAlg::initialize()
{
  // Here you do everything that needs to be done at the very
  //   // beginning on each worker node, e.g. create histograms and output
  //     // trees.  This method gets called before any input files are
  //       // connected.
  ANA_CHECK(m_systematicsList.initialize());


  //Booking histograms
  ANA_CHECK(book(TH1F("j1_pt", "leading jet pt", 100, 0, 0)));
  ANA_CHECK(book(TH1F("j1_eta", "leading jet eta", 50, 0 , 0)));
  ANA_CHECK(book(TH1F("j1_phi", "leading jet phi", 50, 0 , 0)));
  ANA_CHECK(book(TH1F("bj1_pt", "leading bjet pt", 100, 0, 0)));
  ANA_CHECK(book(TH1F("bj1_eta", "leading bjet eta", 50, 0 , 0)));
  
  ANA_CHECK(book(TH1F("num_b", "num_b", 10, 0, 0)));
  ANA_CHECK(book(TH1F("num_lept", "num_lept", 10, 0, 0)));
  ANA_CHECK(book(TH1F("num_bjets", "num_bjets", 10, 0, 0)));
  ANA_CHECK(book(TH1F("num_jets", "num_jets", 10, 0, 0)));
  ANA_CHECK(book(TH1F("num_fjets", "num_fjets", 10, 0, 0)));
  
  ANA_CHECK(book(TH1F("HTb", "HTb", 100, 0, 0)));
  ANA_CHECK(book(TH1F("Et", "Et", 100, 0, 0)));
  ANA_CHECK(book(TH1F("Met", "Met", 100, 0, 0)));
  ANA_CHECK(book(TH2F("hptHtb", "pT(b(4) vs HTb)", 100,0,0,100,0,0)));

//add other info about the histograms like the axes name and all
  return StatusCode::SUCCESS;
}

//========================  (EXECUTE) ================================================================================================

StatusCode VlqNTupleMakerAlg::execute()
{
// events, e.g. read input variables, apply cuts, and fill histograms and trees.  This is where most of your actual analysis code will go.
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

//---------------- MC EVENT WEIGHTS ------------------------------------------
// Use the 0th entry for "nominal" event weight. The scale is luminosity*cross-section
  
  double weight = scale*(eventInfo->mcEventWeights().at(0)); 

//---------------- leptons ----------------------------------------------------
  const xAOD::TruthParticleContainer* electrons = nullptr;
  ANA_CHECK(evtStore()->retrieve(electrons, "TruthElectrons"));
   
  const xAOD::TruthParticleContainer* muons = nullptr;
  ANA_CHECK(evtStore()->retrieve(muons, "TruthMuons"));
  
  int num_lept = electrons->size() + muons->size();

//---------------- bottom quarks ----------------------------------------------------
  const xAOD::TruthParticleContainer* bottoms = nullptr;
  ANA_CHECK(evtStore()->retrieve(bottoms, "TruthBottom"));
   
  num_b(*eventInfo) = bottoms->size();

//Putting Cuts for selection of events : selecting events with 4 bottoms 
     
  if (bottoms->size() < 4) {
    ANA_CHECK(setDefaults());
    ANA_MSG_INFO("Didn't find 4 bottoms");
    return StatusCode::SUCCESS;
  }

//--------------- misseng ET ---------------------------------------------------------  
  const xAOD::MissingETContainer* mets = nullptr;
  ANA_CHECK( evtStore()->retrieve (mets, "MET_Truth") );
  
  Met(*eventInfo) = (*mets)["NonInt"]->met();

  
//--------------- Jets  ---------------------------------------------------------  

  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK( evtStore()->retrieve (jets, "AntiKt4TruthDressedWZJets"));
  float scalarEt = 0.0;
      	for (auto jet : *jets){
	scalarEt += abs(jet->pt());
}
  int num_jets = jets->size();
  Et(*eventInfo) = scalarEt;
 
  

// --------- bjets --------------------------------------------------------------
// The visible Et calculated from bottoms do not match cHTb (calculated below)
 
  double cHTb = 0.0;
  auto bjets = std::make_unique<ConstDataVector<xAOD::JetContainer>>(SG::VIEW_ELEMENTS);
  for (auto jet : *jets){ 
   for (const auto& bottom : *bottoms) {
      double dR = jet->p4().DeltaR(bottom->p4());
      if (dR < 0.4 && jet->pt() > 30000 ) {
          cHTb += jet->pt(); //0.001 factor to convert MeV to GeV
	  
          break;
      }
    }
    bjets->push_back(jet);
  }

 int num_bjets = bjets->size();
  
  HTb(*eventInfo) = cHTb;



// ---------- Basic Event Objects ------------------------------------------------------------  

  TLorentzVector j1 = jets->at(0)->p4();
  TLorentzVector bj1 = bjets->at(0)->p4();
  TLorentzVector bj4 = bjets->at(3)->p4();


// ---------- Event Selction------------------------------------------------------------
/*if (bjets->size() < 4 || cHTb < 900000 ) {
  ANA_CHECK(setDefaults());
  ANA_MSG_INFO("event veto");
  return StatusCode::SUCCESS;
}
*/
// --------- large R jets --------------------------------------------------------------
 
  auto largeRjets = std::make_unique<ConstDataVector<xAOD::JetContainer>>(SG::VIEW_ELEMENTS);
  for (auto jet : *jets){
    for (const auto& bottom : *bottoms) {
      double dR = jet->p4().DeltaR(bottom->p4());
      if (dR < 1.0 && dR > 0.4 && jet->pt() >200e+03){          
    largeRjets->push_back(jet);
  }}}

  int num_fjets = largeRjets->size();

// ---------- Convert units from MeV to GeV --------------------------------------
//          multiply every variable with 0.001 before filling
  Pt_j1(*eventInfo)=j1.Pt()*0.001;
  Eta_j1(*eventInfo)=j1.Eta();
  Et(*eventInfo) *= 0.001;
  Met(*eventInfo) *= 0.001;
  HTb(*eventInfo) *= 0.001;
  auto Pt_bj4=bj4.Pt()*0.001;
  auto bj1_pt=bj1.Pt()*0.001;


// ------------- filling the histogram: Adding weight to each-----------------------------------------------

 
  hist("j1_pt")->Fill(Pt_j1(*eventInfo) , weight );
  hist("j1_eta")->Fill(Eta_j1(*eventInfo) , weight);
  hist("j1_phi")->Fill(j1.Phi(), weight);
  hist("bj1_pt")->Fill(bj1_pt ,  weight);
  hist("bj1_eta")->Fill(bj1.Eta() , weight);

  hist("Met")->Fill(Met(*eventInfo), weight);
  hist("Et")->Fill(Et(*eventInfo), weight);
  hist("HTb")->Fill(HTb(*eventInfo), weight);
  hist("hptHtb")->Fill(Pt_bj4,HTb(*eventInfo));
  
  hist("num_b")->Fill(num_b(*eventInfo), weight) ;
  hist("num_bjets")->Fill(num_bjets, weight);
  hist("num_jets")->Fill(num_jets, weight);
  hist("num_fjets")->Fill(num_fjets, weight);
  hist("num_lept")->Fill(num_lept, weight);


 return StatusCode::SUCCESS;
}



StatusCode VlqNTupleMakerAlg::setDefaults()
{
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
  Pt_j1(*eventInfo) = -999;
  Eta_j1(*eventInfo) = -999;
  Met(*eventInfo) = -999;
  HTb(*eventInfo) = -999;

return StatusCode::SUCCESS;
}


//---------------- (FINALIZE) -----------------------------------------------------------------

StatusCode VlqNTupleMakerAlg::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  //   // called after the last event has been processed on the worker node
  //     // and allows you to finish up any objects you created in
  //       // initialize() before they are written to disk.  This is actually
  //         // fairly rare, since this happens separately for each worker node.
  //           // Most of the time you want to do your post-processing on the
  //             // submission node after all your histogram outputs have been
  //               // merged.
  


return StatusCode::SUCCESS;
}

