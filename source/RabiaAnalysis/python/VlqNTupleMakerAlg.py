from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createService
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
from AsgAnalysisAlgorithms.AsgAnalysisAlgorithmsTest import pileupConfigFiles
from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import makeTriggerAnalysisSequence
from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import makeElectronAnalysisSequence
from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
# Add configurations here

def makeSequence () :

  algSeq = AlgSequence()
  # Algorithm for calculating variables to be dumped
  ntupAlg = createAlgorithm( 'VlqNTupleMakerAlg', 'VlqNTupleMakerAlg' )
  algSeq += ntupAlg



  treename = 'sixtop'

   # Add an ntuple dumper algorithm:


  treeMaker = createAlgorithm( 'CP::TreeMakerAlg', 'TreeMaker' )
  treeMaker.TreeName = treename
  algSeq += treeMaker

  ntupleMaker = createAlgorithm( 'CP::AsgxAODNTupleMakerAlg', 'VlqNTupleMakerEventInfo' )
  ntupleMaker.TreeName = treename
  ntupleMaker.Branches = [ 
              

#                           'EventInfo.Pt_j1     -> Pt_j1',
#                           'EventInfo.Eta_j1     -> Eta_j1',
                           'EventInfo.Met       -> Met',
		           'EventInfo.num_b  -> num_b',
                           'EventInfo.Et        -> Et'                           
                         ]
  algSeq += ntupleMaker

  treeFiller = createAlgorithm( 'CP::TreeFillerAlg', 'TreeFiller' )
  treeFiller.TreeName = treename
  algSeq += treeFiller





  return algSeq
