#ifndef RabiaAnalysis_VlqNTupleMakerAlg_H
#define RabiaAnalysis_VlqNTupleMakerAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <xAODTruth/TruthParticleContainer.h>

class VlqNTupleMakerAlg : public EL::AnaAlgorithm
{
  public:
    // this is a standard algorithm constructor
    VlqNTupleMakerAlg (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;
    

  private:
    StatusCode setDefaults ();
    static bool comparePt (const xAOD::TruthParticle*, const xAOD::TruthParticle*);
//    double CrossSection = 0.04503; //pb - for mb4_400
//    double CrossSection = 0.002965; //pb - for mb4_1000
    double CrossSection = 0.0001029; //pb - for mb4_2000
//    double CrossSection = 7.2974e+02 ; //pb - for ttbar
    double Luminosity = 139e+03; //pb-1
//    double genFiltEff = 4.561919E-01; //ttbar
    double kFactor = 1.0;
//    double scale = Luminosity*CrossSection*genFiltEff*kFactor;  //for ttbar only
    double scale = Luminosity*CrossSection*kFactor; //for vlq signals



    CP::SysListHandle m_systematicsList {this};
};

#endif
