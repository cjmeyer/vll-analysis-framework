# vll-analysis-framework

A framework for studying vector-like leptons

## Quick start

The following should get you up and running:
```
git clone https://gitlab.cern.ch/cjmeyer/vll-analysis-framework.git
cd vll-analysis-framework
mkdir build
source setup.sh

cd build
cmake ../source
make -j12
source x86_64-centos7-gcc11-opt/setup.sh

cd ../run
ana -S H500e350 -a AnalysisFramework.TruthSelectionAlg,MuonTagger.OnnxTestAlg -c dnnntup.json
```

## Command line configuration
The following command-line options are available, and will take precedence over options configured any other way:
```
Usage: ana_eljob.py [options]

Options:
  -h, --help            show this help message and exit
  -s SUBMISSION_DIR, --submission-dir=SUBMISSION_DIR
                        Submission directory for EventLoop
  -a ANALYSIS_SEQUENCE, --analysis-sequence=ANALYSIS_SEQUENCE
                        Sequence of CP and analysis algorithms to schedule
  -i INPUT_DIR, --input-dir=INPUT_DIR
                        Input directory containing files to run over
  -S SAMPLE_NAME, --sample-name=SAMPLE_NAME
                        Name of sample to run over. If both this and -i are
                        passed, -i will take priority.
  -n N_EVENTS_MAX, --n-events-max=N_EVENTS_MAX
                        Maximum number of events to run over.
  -c CONFIG_FILE, --config-file=CONFIG_FILE
                        Configuration options listed in JSON format which will
                        be added to ConfigFlags object.
```

## Configuration file
Any option specified in the config file (passed using `-c` option, see above) that isn't overridden by a command line option will also be available in ConfigFlags during the python configuration.
When appropriate, these values are also used to configure the C++ code through the `declareProperty` interface.

The currently available configuration options are:

| Key | Value | Example |
| --- | ----- | ------- |
| `Input.Files` | List of files to run over | `['/path/to/file1.root', '/path/to/file2.root']` |
| `Input.Directory` | Directory containing files to run over | `/path/to/directory` |
| `Run.AlgorithmSequence` | Comma separated list of algorithms / sequences to run | `AnalysisFramework.TruthSelectionAlg,MuonTagger.NTupleMakerAlg` |
| `Event.GRLs` | List of GRL files to use for selecting data events | `[file1.xml, file2.xml]` |
| `Truth.[Object].ContainerName` | Name of input truth collection | `TruthMuons` |
| `Truth.[Object].PtMin` | Minimum pT of object to consider in MeV | `3e3` |
| `Truth.[Object].EtaMax` | Maximum eta of object to consider | `2.5` |
| `Truth.[Object].RejectCrack` | Specify if objects falling in calorimeter crack should be rejected | `false` |
| `Truth.[Object].CrackEtaMin` | Minimum eta of crack range (only used if rejecting crack objects) | `1.37` |
| `Truth.[Object].CrackEtaMax` | Maximum eta of crack range (only used if rejecting crack objects) | `1.52` |

Note: the currently available [Object] values that can be used for truth selection are `Photons` and `Muons`.

## Included helper sequences and algorithms

The following helper sequences and algorithms are available, and can be specifed in a comma-separated string through the `-a` command-line argument, or as a list in a JSON configuration file.

| Name | Description |
| ---- | ----------- |
| `AnalysisFramework.BasicEventSelectionSeq` | Apply primary vertex requirement, GRL (for data), and pileup weight (for MC) |
| `AnalysisFramework.RecoObjectsSeq` | Build calibrated, selected and pT-sorted containers for standard reco objects (`AnalysisPhotons`, `AnalysisMuons`, etc.) |
| `AnalysisFramework.TruthObjectsSeq` | Build selected (pT, eta) and pT-sorted containers for standard truth objects (`AnalysisTruthPhotons`, `AnalysisTruthMuons`) |
| `AnalysisFramework.TruthSelectionAlg` | Build selected (pT, eta, origin) and pT-sorted containers for truth objects (`VllTruthPhotons`, `VllTruthMuons`). Deprecated, but might be a useful example. |

Other example algorithms include:

| Name | Description |
| ---- | ----------- |
| `MuonTagger.NTupleMakerAlg` | Make flat ntuple containing variables used to train DNN muon tagger. Currently needs `AnalysisFramework.TruthSelectionAlg` to run first. |

## Short names for samples

In addition to pointing at a directory containing the files you'd like to run over (using `-i` or `Input.Directory`) the file `AnalysisFramework/python/Samples.py` contains a map of short sample names and associated directories.
This allows you to specify a sample using the `-S [short name]` command-line option.
For example:
```
ana -S mmgam
```
will run over the input directory `/P/bolt/data/cjmey/share/data/mc20_13TeV.700019.Sh_228_mmgamma_pty7_ptV90.deriv.DAOD_PHYS.e7947_s3681_r13145_r13146_p5057`.

## Making your own analysis algorithm

To run your own analysis algorithm, you need to create your own versions of the following three files, preferably in an appropriate sub-package directory (e.g. MuonTagger):
```
AnalysisFramework/Root/ExampleAnalysisAlg.cxx
AnalysisFramework/AnalysisFramework/ExampleAnalysisAlg.h
AnalysisFramework/python/ExampleAnalysisAlg.py
```
You will also need to modify the equivalent of the following files in the sub-packge your algorithm lives in so that the proper class dictionaries are compiled:
```
AnalysisFramework/AnalysisFramework/AnalysisFrameworkDict.h
AnalysisFramework/AnalysisFramework/selection.xml
```
In order to run your algorithm, you can then issue a command like:
```
ana -S mmgam -a AnalysisFramework.ExampleAnalysisAlg
```
Note: depending on what containers your algorithm requires, you may need to schedule other algorithms to run first by adding to the `-a` option, e.g.:
```
ana -S mmgam -a AnalysisExample.BasicEventSelectionAlg,AnalysisExample.RecoObjectsSeq,AnalysisFramework.ExampleAnalysisAlg -c vllbase.json
```
You can also add these algorithms to the `Run.AlgorithmSequence` key of any JSON configuration file you might be using.

## How to develop

Since we are (currently) a small group, we can all work from the same repository, i.e., no need to fork.
(Although you are welcome to.)
When working locally, you *should* make a branch though, so that changes can be tracked and you can eventually make a merge request.
The general flow looks something like:
```
git clone https://gitlab.cern.ch/cjmeyer/vll-analysis-framework.git
cd vll-analysis-framework
git checkout -b my-topic-branch origin/master # choose a more appropriate name than my-topic-branch

# Make your updates

git add [files changed]
git commit # include descriptive commit message
git push origin my-topic-branch
# Follow the link printed on the screen for creating a MR via web interface
```
We can all update the code to benefit from the development of others.
However, it's generally good practice to have at least one person look over it before merging to `master` in order to catch bugs, make comments on readability, etc.

# Using VS Code Integrations

If you use VS Code the configurations for IntelliSense and Python/C++ code formatting are included in the repository.
You will need to clone the respository, make a `build` directory, source the `setup.sh` script, and call `cmake ../source` from within the build directory to generate everything needed by VS Code.
Once this is done, you can connect to the top-level repository directory within VS Code, e.g., `vll-analysis-framework`.
For everything to work well, you will also want to install the following extensions:
* ms-vscode.cpptools
* ms-python.python
* ms-python.autopep8
* ms-python.flake8
* twxs.cmake

It may take a minute or two for the IntelliSense scan to complete each time you login, but once it does auto-complete will list all possible header files to include, all functions a class contains, etc.
