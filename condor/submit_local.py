#!/usr/bin/env python
import os
from AnalysisFramework import condor

pwd = os.environ["PWD"]
nfs_area = "/P/bolt/data/" + os.environ["USER"] + "/condor"

# Job setup
configstem = "vllbase" # don't include .json
executable = f"ana -l input_data.txt -c {configstem}.json"

# Configuration common to all jobs
basejob = condor.JobSpec()
basejob.job_inputs = [f"{pwd}/../run/{configstem}.json"]
basejob.job_setup = f"{pwd}/../setup.sh"
basejob.job_libs  = os.environ["TestArea"] + "/" + os.environ["AnalysisBase_PLATFORM"]
basejob.output_dir = nfs_area+"/out/"+configstem

helper = condor.CondorSubmit(cleanOldJobs=True, inputSamples="local")

# Samples to run over
ntup_path = "/P/bolt/data/cjmey/share/VectorLikeLeptonsBkgSamples/"
samples = {
  "mc20e" : {
    "Zmm" : "Zmumu/mc20_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_PHYS.e3601_s3681_r13167_r13146_p5267",
    "ZZ"  : "SM_ZZ/mc20_13TeV.361603.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZllll_mll4.deriv.DAOD_PHYS.e4475_s3681_r13167_r13146_p5267",
  },
}

for dtype in samples:
  for sample in samples[dtype]:
    job = basejob

    name = dtype + "_" + sample
    job.name = name
    job.job_area = nfs_area + "/log/" + configstem + "/" + name

    job.input_dir = ntup_path + "/" + samples[dtype][sample]
    job.run_command = executable

    helper.submitJob(job)
