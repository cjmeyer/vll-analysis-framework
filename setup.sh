export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

version='24.2.28'

cd build

lsetup git "asetup AnalysisBase,${version}"


[ -d $ATLAS_LOCAL_ROOT_ARCH-* ] \
  && echo "Going to add local project libraries to path" \
  && source $ATLAS_LOCAL_ROOT_ARCH-*/setup.sh

cd ../

alias b='cd $TestArea; make -j12; cd -'
alias cmake="cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=TRUE -DATLAS_ENABLE_IDE_HELPERS=TRUE"
export CXXFLAGS="-Werror"

